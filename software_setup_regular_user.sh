#!/bin/bash
#su rsd1602
echo "Updating ROS"
rosdep init
rosdep update
echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc

#Installing Frobomind
echo "Installing Frobomind"
mkdir -p ~/roswork/src
cd ~/roswork/src
#git clone http://github.com/frobolab/frobomind.git
git clone http://github.com/niive12/frobomind.git
git clone http://github.com/frobolab/frobit_demo.git


#Fixed by Kjeld during October.
#Fix bug in frobomind (transverse_mercator scripts are in wrong place)
#shopt -s extglob
#mkdir -p ~/roswork/src/frobomind/fmLib/math/geographics/transverse_mercator/scripts
#mv ~/roswork/src/frobomind/fmLib/math/geographics/transverse_mercator/src/transverse_mercator_py/!(__init__.py)  ~/roswork/src/frobomind/fmLib/math/geographics/transverse_mercator/scripts

#Build frobit_demo and required packages
cd ~/roswork/src
frobomind/frobomind_make cleanall
frobomind/frobomind_make -i /frobit_demo
#One more time for the dependencies!
frobomind/frobomind_make -i /frobit_demo

#Put ros stuff in global environment
echo "source ~/roswork/devel/setup.bash" >> ~/.bashrc
echo 'PATH="$PATH:~/roswork/src"' >> ~/.bashrc
source ~/.bashrc
